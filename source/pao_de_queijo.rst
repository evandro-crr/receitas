.. |check| raw:: html

    <input type="checkbox">

Pãozão de Queijo
================

.. Warning:: Adicione o polvilho por último.

Ingredientes
------------

- |check| 2 xícaras de leite
- |check| 1 xícara de óleo
- |check| ½ colher rasa de sal
- |check| 500g de polvilho doce
- |check| 2 ovos
- |check| A quantidade máxima de queijo parmesão ralado que conseguir 

Modo de Preparo
---------------

1. Bata tudo no liquidificador.
2. Coloque a massa em uma forma untada e leve ao forno pré-aquecido por 35 minutos.
