Receitas
========

Livro pessoal de receitas.

.. note:: 

   Para não ter que pesquisar na internet toda vez que for cozinhar alguma coisa.

.. toctree::
   :maxdepth: 1
   :caption: Receitas:
   
   pao_de_queijo
   panqueca
   strogonoff
   bolo_de_fuba
