.. |check| raw:: html

    <input type="checkbox">

Panquecas
=========

Massa
-----

Ingredientes
^^^^^^^^^^^^

* |check| 3 xícaras de leite
* |check| 4 ovos
* |check| 8 colheres de óleo
* |check| 1 colher de sal
* |check| 3 xícaras de farinha de trigo
* |check| 1 cebola roxa
* |check| Açafrão

.. note:: A massa deve ficar bem mole.

Modo de Preparo
^^^^^^^^^^^^^^^

1. Bata tudo no liquidificador.
2. Unte a forma e aqueça bem antes de colocar a massa.
3. Bata a massa novamente ocasionalmente.

Recheio
-------

.. warning:: Parte da Pati.

Ingredientes
^^^^^^^^^^^^

* |check| Brócolis
* |check| 1 caixa de creme de leite
* |check| Cebola roxa
* |check| Queijo gorgonzola
* |check| Sal
* |check| Temperos (a gosto)

Modo de Preparo
^^^^^^^^^^^^^^^

1. Primeiro, coloque o brócolis para cozinhar. Em outra panela, doure a cebola.
2. Em seguida, acrescente o brócolis já cozido e a caixa de creme de leite.
3. Misture bem os ingredientes. Por fim, adicione o queijo cortado em cubinhos, ajuste o sal e tempere a gosto.

.. note:: Separe um pouco do recheio e adicione carne moída.
