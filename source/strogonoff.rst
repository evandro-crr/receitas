.. |check| raw:: html

    <input type="checkbox">
    
Strogonoff
==========

Ingredientes
^^^^^^^^^^^^

* |check| Óleo de soja
* |check| Cebola roxa
* |check| Alho
* |check| Tiras de carne
* |check| 1x Creme de leite
* |check| 3x Colheres de molho de tomate
* |check| Açafrão
* |check| Sal
* |check| Pitada de açúcar

Modo de Preparo
^^^^^^^^^^^^^^^

1. Bata o alho e a cebola roxa no mixer.
2. Frite-os com o óleo e adicione a carne.
3. Quando a carne estiver seca, adicione o creme de leite e o molho de tomate.
4. Tempere a gosto e deixe o caldo engrossar.
