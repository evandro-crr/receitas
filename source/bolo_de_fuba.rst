.. |check| raw:: html

    <input type="checkbox">
    
Bolo de Fubá
============

Ingredientes
------------

- |check| 3 ovos
- |check| 1 copo de leite
- |check| 1½ copo de açúcar
- |check| 50g de coco ralado
- |check| ½ copo de óleo
- |check| 2 copos de farinha de milho (fubá)

Modo de Preparo
---------------

1. Bata todos os ingredientes no liquidificador.
2. Unte uma assadeira redonda.
3. Leve ao forno pré-aquecido por 40 minutos.